function exam(numArr,target){
    if(!Array.isArray(numArr) && numArr.length <= 0)
        return 'Numbers needs to be array';

    for(var i = 0; i < numArr.length ;i++){
        var currentNumber = numArr[i];
        for(var idx = 0; idx < numArr.length ;idx++){
            if(idx !== i){//Index should not be equal to the current index
                var compareNumber = numArr[idx];
                var sum = currentNumber + compareNumber;
                if(sum === target ){
                    return [i,idx];
                }
            }            
        }
    }
    return 'Target not found on the given numbers';
}

console.log(exam([2, 3,  7, 11, 15],9))
console.log(exam([1, 2, 3, 4, 5],6))